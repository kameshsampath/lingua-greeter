# Lingua Greeter

As part of the demo, let us deploy a Kubernetes application called `lingua-greeter`. The application exposes a REST API `/:lang` , that allows you to translate a text `Hello World!` into the language `:lang` using Google Translate client.

> **NOTE**:
> The `:lang` is the BCP 47 language code, <https://en.wikipedia.org/wiki/IETF_language_tag>.

## Prerequisites

- [Google Cloud Account](https://cloud.google.com) with Google Translation API enabled.
- [direnv](https://direnv.net)

## Run and Test Application

### Environment Variables

When working with Google Cloud the following environment variables helps in setting the right Google Cloud context like Service Account Key file, project etc., You can use [direnv](https://direnv.net) or set the following variables on your shell,

```shell
export GOOGLE_APPLICATION_CREDENTIALS="the google cloud service account key json file to use"
```

(e.g.)

```shell
export GOOGLE_APPLICATION_CREDENTIALS=~/.ssh/my-sa-key.json
```

> **TIP** If you are using direnv you can then create file `.envrc.local` and add the environment variables. They can then be loaded using `direnv allow .`

You can find more information about gcloud cli configurations at <https://cloud.google.com/sdk/docs/configurations>.

```shell
go run main.go
```

Cal the service,

```shell
curl "http://localhost:8080/?name=Newton&lang=ta"
```

The service should succeed with a response,

```json
{"Name":"Newton","Lang":"ta","Message":"வணக்கம், நியூட்டன்"}
```

If you have a Kubernetes Cluster then you can deploy the application using the command,

```shell
kubectl -k config/
```
