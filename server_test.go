package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestGreetNamed(t *testing.T) {
	greeting := Greetings{
		Name:    "Kamesh",
		Message: "வணக்கம் காமேஷ்",
	}
	// Setup
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/?name=Kamesh", nil)
	req.Header.Add("Content-Type", "application/json")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	// Assertions
	if assert.NoError(t, greet(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		var got Greetings
		err := json.Unmarshal(rec.Body.Bytes(), &got)
		assert.NoError(t, err)
		assert.Equalf(t, greeting.Message, got.Message, "Expecting %s but got %s", greeting.Message, got.Message)
	}
}
