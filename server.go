package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"cloud.google.com/go/translate"
	"github.com/caarlos0/env/v8"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"golang.org/x/text/language"
	"google.golang.org/api/option"
)

type Greetings struct {
	APIKey  string `env:"API_KEY" json:"-"`
	Name    string `query:"name"`
	Lang    string `query:"lang"`
	Message string
}

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	port := "8080"
	if p, ok := os.LookupEnv("PORT"); ok {
		port = p
	}

	e.GET("/", greet)

	if err := e.Start(":" + port); err != nil {
		log.Fatal(err)
	}
}

func greet(c echo.Context) error {
	var greeting Greetings
	err := env.Parse(&greeting)
	if err != nil {
		return err
	}

	err = c.Bind(&greeting)
	if err != nil {
		return err
	}

	if greeting.Name == "" {
		greeting.Name = "Anonymous"
	}

	if greeting.Lang == "" {
		greeting.Lang = language.Tamil.String()
	}

	err = translateGreeting(&greeting)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, greeting)
}

func translateGreeting(g *Greetings) error {
	m := fmt.Sprintf("Hello, %s", g.Name)

	bgCtx := context.Background()
	ctx, cancel := context.WithTimeout(bgCtx, 10*time.Second)
	defer cancel()

	tl, err := language.Parse(g.Lang)
	if err != nil {
		return fmt.Errorf("language.Parse: %v", err)
	}

	client, err := translate.NewClient(bgCtx, option.WithAPIKey(g.APIKey))
	if err != nil {
		return err
	}
	defer client.Close()

	resp, err := client.Translate(ctx, []string{m}, tl, nil)

	if err != nil {
		return err
	}

	if len(resp) > 0 {
		g.Message = resp[0].Text
	} else {
		return fmt.Errorf("translation of %s returned empty response", m)
	}

	return nil
}
